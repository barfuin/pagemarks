#
# pagemarks - Free, git-backed, self-hosted bookmarks
# Copyright (c) 2019-2021 the pagemarks contributors
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License, version 3, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/gpl.html>.
#

variables:
    LC_ALL: 'C.UTF-8'

workflow:
    rules:
        - if: $CI_MERGE_REQUEST_IID
        - if: $CI_COMMIT_TAG
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

default:
    before_script:
        - 'python -V'
        - 'pip -V'
        - 'node -v || echo "node not present"'
        - 'npm -v || echo "npm not present"'


test:
    image: '${CI_REGISTRY_IMAGE}/ci:latest'
    stage: test
    script:
        # Python Setup
        - 'echo -e "\e[0Ksection_start:`date +%s`:python_setup[collapsed=true]\r\e[0KSetup Python Environment"'
        - 'virtualenv venv'
        - 'source venv/bin/activate'
        - 'pip install -r requirements.txt'
        - 'echo -e "\e[0Ksection_end:`date +%s`:python_setup\r\e[0K"'
        - 'python prepare.py'
        - 'pip install -e .'
        # Node Tests
        - 'npm test'
        - 'npm run test-xml'
        # Python Tests
        - 'coverage run --branch -m xmlrunner discover test -o build/reports'
    artifacts:
        expire_in: 1 day
        paths:
            - build/
            - pagemarks/resources/css/
            - pagemarks/js/vendor/
            - venv/
        reports:
            junit:
                - build/TEST-*.xml
                - build/reports/TEST-*.xml
    rules:
        - when: on_success

release:
    image: 'python:3.10-slim-bullseye'
    stage: deploy
    needs: ['test']
    script:
        - 'echo -e "\e[0Ksection_start:`date +%s`:python_setup[collapsed=true]\r\e[0KSetup Python Environment"'
        - 'rm -rf venv/'
        - 'virtualenv venv'
        - 'source venv/bin/activate'
        - 'pip install -r requirements.txt'
        - 'echo -e "\e[0Ksection_end:`date +%s`:python_setup\r\e[0K"'
        - 'python -m build'
        - >-
            TWINE_USERNAME=__token__ TWINE_PASSWORD=${PYPI_TOKEN}
            python -m twine upload --repository-url https://upload.pypi.org/legacy/ dist/*
    rules:
        - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^v[0-9][0-9.]+$/'


pages:
    image: '${CI_REGISTRY_IMAGE}/ci:latest'
    stage: deploy
    needs: ['test']
    variables:
        PAGEMARKS_REPO: demo_repo
    script:
        - 'source venv/bin/activate'
        - 'git -C ${PAGEMARKS_REPO} init'
        - 'git -C ${PAGEMARKS_REPO} config --local user.name "${GITLAB_USER_NAME}"'
        - 'git -C ${PAGEMARKS_REPO} config --local user.email ${GITLAB_USER_EMAIL}'
        - 'git -C ${PAGEMARKS_REPO} add \*'
        - 'git -C ${PAGEMARKS_REPO} commit -m "Gitify temporarily"'
        - 'pip install -e .'
        - 'pagemarks --no-pull build --demo --base-url /pagemarks --gitlab-id ${CI_PROJECT_ID} --output-dir public'
        - 'find public/'
    artifacts:
        paths:
            - public
    rules:
        - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
